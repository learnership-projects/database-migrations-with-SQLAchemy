# Database migrations with SQLAlchemy 

## How to run this project:

This project follows the Git feature branching workflow, necessitating switching between branches when executing scripts against our databases. For more info about the purpose of this project, you can check it out in the `production_documentation.md`.

1. **Open your VSCode, go to your Ubuntu terminal and create your virtual environment:**
```
pipenv install
```
2. **Activate your virtual environment:**
```
pipenv shell
```
3. **You need to install all your dependencies to your virtual environment so that you can run this package, to do that run the following commands in your Ubuntu terminal:** 
```
pipenv install -r requirements.txt
```
4. **After, install the packages in the development mode. This will be installed in your virtual environment.**
```
python setup.py develop
```
5. **Commit your secret password**
- Before running from this point onwards, we need to create a shell script to utilise environment variables and store our secret passwords and credentials in shell scripts for both `production` databases.

Now create a shell for the `production` database called `prod_credential.sh`, and copy and paste the following content into it:
```
#\!/bin/sh

export PROD_DB_USER=ENTER_DB_USER_NAME
export PROD_DB_PASS=ENTER_DB_PASS_NAME
export PROD_DB_HOST=localhost
export PROD_DB_NAME=production
```
Replace `ENTER_DB_USER_NAME` and `ENTER_DB_PASS_NAME` with your actual database username and password in these two environment variables. Run this shell script, and make sure you have the `production` environment variables:
```
source prod_credentials.sh
``` 

6. **Now you need to download and pull your Docker image, and run it using the following commands in your Ubuntu terminal:**
```
docker-compose -f docker-compose-prod.yml up -d
```

7. **To successfully run our migrations, make sure that the SQLalchemy URL for `production` databases in the `alembic.ini` is active. After let’s upgrade the `production` database:**
```
alembic upgrade heads
```
8. **The script `create_c28_learners.py` has been created, run it against our `production` database and add 5 random C28 learners to the `production` database.**
```
python src/create_c28_learners.py
``` 
10. **To query all learners from the `production` database, run the following commands:**
```
python src/query_database.py
```
You will get the output of the learners added to your database on the screen.

10. **Once you are done, stop and remove the docker image inside the docker using the docker-compose file.**

Remove the image from docker by running the following commands:
```
docker-compose -f docker-compose-prod.yml down
```

- [x] This process was performed successfully.
