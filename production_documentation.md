# Documentation of production database
___

## What's the purpose of this project:

### An overview of the databases and their columns:
In this project, two databases have been created: `production` and `dev`. The `dev` database is intended for experimentation and development, and developers should exercise caution to avoid deleting or corrupting any data in the `production` database.

Both databases share the following common columns:

1. id_number: This is a primary key column containing integer values representing the ordered numbers of each learner.This column was deleted in third situation.
2. first_name: This column stores the first names of learners, and values are expected to be strings of characters.
3. surname: This column stores the surnames of learners, and values are expected to be strings of characters.
4. chatname: This column stores the chatnames of learners, and values are expected to be strings of characters. This column will be renamed to rocketchat_user.
5. github_name: This column stores the GitHub usernames of learners, and values are expected to be strings of characters.
6. personal_email_address: This column stores the personal email addresses of learners, and values are expected to be strings of characters.
7. cohort: This column stores the cohort numbers of learners, and values are expected to be integers.

1. **Situation 0 :Using your dev database:*
- Write some sql alchemy models to describe a learner. A learner has the following attributes:
    + first name
    + surname
    + chatname
    + github name
    + id number

    + Create some migrations and run them against your dev database. Take a look inside the migration files.
    + Create a “create_learners.py” that adds some new learners to the database and run it against your dev database.
    + Add a new column to your sqlalchemy model for personal_email_address. Set it to be a required field, and must be unique.
    + Make your migrations again and run them. This step won’t go smoothly - you’ll need to do it in a few steps.
    + Look at your migration files, they should make sense.

Using your production database:
    + Run all your migrations.
    + Update your create_learners script to add personal_email_address values, then run it against the prod database.
    This should have been quite smooth.


Back to the dev db

2. **Situation 1: A new column**
Using your dev db:

    + Open up a psql shell and add a new column to your database called “cohort”. Manually update the data - everyone is in “C25 Data Eng”
    + Open a python shell and use your models to query the data. What happened?
    + Now create your migrations and look at the changes. Are there any changes? What will happen if you run your migrations?
    + Now go to your models and add a required cohort column.
    + Create your migrations and look at the changes. Did anything happen?
    + Now make a new script called “create_c26_learners.py”. Use it to add 5 random C26 learners to your database.
    + Run your script on your development database.
    + Now run your migrations against your prod database. What happened?
    + Run create_c26_learners against your prod database. What happened?
Do whatever it takes to get it to work… you might need to checkout an earlier commit. At the end, you’ll have the C26 learners in the prod database.

Rules: Don’t delete any data in your production database! But you can completely delete your dev database if you wanted to.

3. **Situation 2: A renamed column**
Using your dev db:

    + Open up a psql shell and rename the “chatname” column to “rocketchat_user”.
    + Open up a python shell and try to query your db. What happened?
    + Now create your migrations and look at the changes. Are there any changes? What will happen if you run your migrations?
    + Now go to your models and rename the chatname column.
    + Create your migrations and look at the changes. Did anything happen?
    + Create a new script to create some C27s and run it against your dev db.
    + Run your script on your development database.
    + Now run your migrations against your prod database. What happened?
    + Run create_c27_learners against your prod database. What happened?

Do whatever it takes to get it to work… you might need to checkout an earlier commit. By the end you’ll have the C27 learners in the prod database.

Rules: Don’t delete any data in your production database! But you can completely delete your dev database if you wanted to.

4. **Situation 3: A deleted column**
Using your dev db:

    + Open up a psql shell and delete the id number column.
    + Open up a python shell and try to query your db. what happened?
    + Now create your migrations and look at the changes. Are there any changes? What will happen if you run your migrations?
    + Now go to your models and remove the column.
    + Create your migrations and look at the changes. Did anything happen?
    + Create a new script to create some C28s and run it against your dev db.
    + Run your script on your development database.
    + Now run your migrations against your prod database. What happened?
    + Run create_c28_learners against your prod database. What happened?

Do whatever it takes to get it to work… you might need to checkout an earlier commit. By the end you’ll have the C28 learners in the prod database.

Rules: Don’t delete any data in your production database! But you can completely delete your dev database if you wanted to.


## Migrations environment folder:
___
I have created an environment and generated a migration directory named `migrations` by running the following commands.:
```
alembic init migrations
```
I have committed changes for this environment. Fortunately, not all users need to create this migrations environment.

1. The `alembic.ini` file has been edited and the `sqlalchemy.url`s have been replaced with the following URL:

For the production environment:
```
sqlalchemy.url = postgresql://${PROD_DB_USER}:${PROD_DB_PASS}@${PROD_DB_HOST}:${PROD_DB_HOST}/${PROD_DB_NAME}
```

2. The `env.py` file has been edited: 
- By uncommenting and changing the lines 19-21 with the following code:
This line:
```
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
# target_metadata = None
```
changed to:
```
from src.models import Base
target_metadata = Base.metadata
# target_metadata = None
``` 
- The function `run_migrations_online` has been modified to this:

```
def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    # connectable = engine_from_config(
    #     config.get_section(config.config_ini_section, {}),
    #     prefix="sqlalchemy.",
    #     poolclass=pool.NullPool,
    # )
    
    from sqlalchemy import create_engine
    import re
    import os

    url_tokens = {
        "PROD_DB_USER": os.getenv("PROD_DB_USER", ""),
        "PROD_DB_PASS": os.getenv("PROD_DB_PASS", ""),
        "PROD_DB_HOST": os.getenv("PROD_DB_HOST", ""),
        "PROD_DB_NAME": os.getenv("PROD_DB_NAME", "")
    }

    url = config.get_main_option("sqlalchemy.url")
    url = re.sub(r"\${(.+?)}", lambda m: url_tokens[m.group(1)], url)

    connectable = create_engine(url)

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata,
            compare_type=True,
            compare_server_default=True,
        )

        with context.begin_transaction():
            context.run_migrations()


``` 
The credentials have been included in all scripts as required.

3. Now with the environment in place, the new revision script has been created using `alembic revision` commands in conjunction with the `--autogenerate` option:
```
alembic revision --autogenerate -m "create learners table"
```

4. You can look for more information on how to run this project in the `README.md`
