from setuptools import setup, find_packages

setup(
    name="src",
    version="1.0.0",
    decription="This is the package that contains models for learners of different cohort numbers at company that trains top talent digital learners",
    author="Kholofelo Moropane",
    packages=find_packages(),
)