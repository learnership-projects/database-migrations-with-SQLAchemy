from src.models import Learners

if __name__ == "__main__":
    learner_6 = Learners(
        6382, "atang", "moropane", "atang.moropane", "atlim", "C26 Data Eng"
    )
    learner_7 = Learners(
        7726, "goodwin", "mosate", "goodwin.mosate", "goodWillm", "C26 Data Eng"
    )
    learner_8 = Learners(
        8634, "lucas", "monareng", "lucas.monareng", "luMona", "C26 Data Eng"
    )
    learner_9 = Learners(
        9549, "thakgi", "monna", "thakgi.monna", "thakgiMan", "C26 Data Eng"
    )
    learner_10 = Learners(
        10110, "itumeleng", "nari", "itumeleng.nari", "ituNare", "C26 Data Eng"
    )

    learners = [learner_6, learner_7, learner_8, learner_9, learner_10]

    Learners.add_learners_to_database(learners)
