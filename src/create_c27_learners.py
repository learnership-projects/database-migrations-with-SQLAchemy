from src.models import Learners

if __name__ == "__main__":
    learner_11 = Learners(
        11382, "Eunicca", "mahwa", "Eunicca.mahwa", "atlim", "C27 Data Eng"
    )
    learner_12 = Learners(
        123726, "Ryan", "mosate", "Ryan.mosate", "Ryanm", "C27 Data Eng"
    )
    learner_13 = Learners(
        138634,
        "Collen",
        "moropane",
        "Collen.moropane",
        "CollenMoropane",
        "C27 Data Eng",
    )
    learner_14 = Learners(
        149549, "Kamogelo", "maloma", "Kamogelo.maloma", "KamogeloMan", "C27 Data Eng"
    )
    learner_15 = Learners(
        151110, "tsotso", "mashau", "tsotso.mashau", "tsotsomashau", "C27 Data Eng"
    )

    learners = [learner_11, learner_12, learner_13, learner_14, learner_15]

    Learners.add_learners_to_database(learners)
