from src.models import Learners

if __name__ == "__main__":
    learner_16 = Learners("Vanny", "makua", "Vanny.makua", "Vanny.m", "C28 Data Eng")
    learner_17 = Learners(
        "John", "Tsesane", "John.Tsesane", "john.tsesane", "C28 Data Eng"
    )
    learner_18 = Learners(
        "morasei", "molamo", "Collen.molamo", "Collenmolamo", "C28 Data Eng"
    )
    learner_19 = Learners(
        "Mpokoto", "moropa", "Mpokoto.moropa", "MpokotoMan", "C28 Data Eng"
    )
    learner_20 = Learners(
        "Charlotte", "mokau", "Charlotte.mokau", "Charlottemokau", "C28 Data Eng"
    )

    learners = [learner_16, learner_17, learner_18, learner_19, learner_20]

    Learners.add_learners_to_database(learners)
