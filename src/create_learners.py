from src.models import Learners

if __name__ == "__main__":
    learner_1 = Learners(
        4311, "kholofelo", "moropane", "kholofelo.moropane", "rkmoropane"
    )
    learner_2 = Learners(
        2126,
        "daphney",
        "maloma",
        "daphney.maloma",
        "maDaph",
    )
    learner_3 = Learners(3234, "kgami", "matlala", "kgami.matlala", "kgamilele")
    learner_4 = Learners(
        4549, "thakgatso", "mohlala", "thakgatso.mohlala", "thakgaletswalom"
    )
    learner_5 = Learners(5110, "itumeleng", "moukangwe", "itumeleng.moukangwe", "ituM")

    learners = [learner_1, learner_2, learner_3, learner_4, learner_5]

    Learners.add_learners_to_database(learners)
