import os

from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import declarative_base, sessionmaker


PROD_DB_USER = os.getenv("PROD_DB_USER", "")
PROD_DB_PASS = os.getenv("PROD_DB_PASS", "")
PROD_DB_HOST = os.getenv("PROD_DB_HOST", "")
PROD_DB_NAME = os.getenv("PROD_DB_NAME", "")

Base = declarative_base()


class Learners(Base):
    __tablename__ = "learners"

    id = Column(Integer, primary_key=True)
    first_name = Column(String(50), nullable=False, server_default="")
    surname = Column(String(50), nullable=False, server_default="")
    rocketchat_user = Column(String(50), nullable=False, server_default="")
    github_name = Column(String(50), nullable=False, server_default="")
    personal_email_address = Column(
        String(100), unique=True, nullable=False, server_default=""
    )
    cohort = Column(String(100), nullable=False, server_default="")

    def __init__(
        self, first_name, surname, rocketchat_user, github_name, cohort=""
    ) -> None:
        self.first_name = first_name
        self.surname = surname
        self.rocketchat_user = rocketchat_user
        self.github_name = github_name
        self.personal_email_address = self.set_personal_email_address()
        self.cohort = cohort

    def set_personal_email_address(self):
        return f"{self.first_name.lower()}.{self.surname.lower()}@SQLalchemy.com"

    def __repr__(self) -> str:
        return f"(id='{self.id}', first_name='{self.first_name}',  surname='{self.surname}' with rocketchat_user='{self.rocketchat_user}' and github_name='{self.github_name}' has email address='{self.personal_email_address}' is in cohort='{self.cohort}'.)"

    @staticmethod
    def _create_engine_using_env_vars():
        engine = create_engine(
            f"postgresql://{PROD_DB_USER}:{PROD_DB_PASS}@{PROD_DB_HOST}:5432/{PROD_DB_NAME}",
            echo=True,
        )

        return engine

    @classmethod
    def _get_session_maker(cls):
        engine = cls._create_engine_using_env_vars()
        Base.metadata.create_all(bind=engine)
        session_maker = sessionmaker(bind=engine)

        return session_maker()

    @classmethod
    def add_learners_to_database(cls, learners):
        session = cls._get_session_maker()
        for learner in learners:
            session.add(learner)
        session.commit()

    @classmethod
    def get_learners_from_database(cls):
        session = cls._get_session_maker()
        all_learners = session.query(Learners).all()

        for learner in all_learners:
            print(learner)
